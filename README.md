**The journey of creating a selfsustained electronic tap valve based on motion sensing.**
-------------------------------------
_The projects name is Wave2Wash and so it begins. The whole idea is that we waste too much water on a daily basis. The infographics which I 'll skip for now show that we barely actually use half of the water we consume with the other half being wasted. So a good start is a sensor based system that tells the tap when we genuinally want to use the water and when we don't._

The following things should be taken into consideration before creating such a device:
- water pressure 
- antilimescale materials (abs)
- self sustainability 
- dynamo 
- hardness and durability of components plus degradation and health related issues 
- IP rating and quality reassurance 
- possible troubleshooting plus easy to fix
- WiFi/bluetooth extension for statistics respective advice and timing settings+modes 
- plug'n'play 
- before flight thingy to prevent drainage of the battery
- compact and beautiful 
- compatibility with many types of taps (designing variants)

The research includes:
- 3D printing ABS
- Fusion360
- designing water dynamo (hydroturbine)
- low profile rechargeable battery (3V)
- water minivalve
- inhesive for water resistibility
- proper IR sensors and placing
- DC DC LP converter
- selection based on low consumption ICs
- battery protection
